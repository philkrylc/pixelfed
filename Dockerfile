FROM cloudron/base:2.0.0@sha256:f9fea80513aa7c92fe2e7bf3978b54c8ac5222f47a9a32a7f8833edf0eb5a4f4

RUN mkdir -p /app/code
WORKDIR /app/code

# keep composer at the end since it installs cli and chooses 7.4 as the alternative if in the front
RUN apt remove -y php* && \
    add-apt-repository -y ppa:ondrej/php && apt-get update -y && \
    apt-get install -y php7.3 libapache2-mod-php7.3 crudini \
    php7.3-redis \
    php7.3-apcu \
    php7.3-bcmath \
    php7.3-bz2 \
    php7.3-curl \
    php7.3-dba \
    php7.3-enchant \
    php7.3-gd \
    php7.3-geoip \
    php7.3-gettext \
    php7.3-imagick \
    php7.3-imap \
    php7.3-intl \
    php7.3-json \
    php7.3-ldap \
    php7.3-mbstring \
    php7.3-mysql \
    php7.3-pgsql \
    php7.3-readline \
    php7.3-soap \
    php7.3-sqlite3 \
    php7.3-tidy \
    php7.3-uuid \
    php7.3-xml \
    php7.3-zip \
    cron \
    apache2-dev \
    build-essential && \
    apt install -y composer && \
    rm -rf /var/cache/apt /var/lib/apt/lists /etc/ssh_host_*

RUN apt autoremove -y

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf
COPY apache/lamp.conf /etc/apache2/sites-enabled/lamp.conf
RUN echo "Listen 80" > /etc/apache2/ports.conf
RUN a2disconf other-vhosts-access-log
RUN a2enmod rewrite headers rewrite expires cache

# configure mod_php
RUN crudini --set /etc/php/7.3/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/7.3/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/7.3/apache2/php.ini PHP memory_limit 128M && \
    crudini --set /etc/php/7.3/apache2/php.ini Session session.save_path /run/app/sessions && \
    crudini --set /etc/php/7.3/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/7.3/apache2/php.ini Session session.gc_divisor 100

# install RPAF module to override HTTPS, SERVER_PORT, HTTP_HOST based on reverse proxy headers
# https://www.digitalocean.com/community/tutorials/how-to-configure-nginx-as-a-web-server-and-reverse-proxy-for-apache-on-one-ubuntu-16-04-server
RUN mkdir /app/code/rpaf && \
    curl -L https://github.com/gnif/mod_rpaf/tarball/669c3d2ba72228134ae5832c8cf908d11ecdd770 | tar -C /app/code/rpaf -xz --strip-components 1 -f -  && \
    cd /app/code/rpaf && \
    make && \
    make install && \
    rm -rf /app/code/rpaf

# configure rpaf
RUN echo "LoadModule rpaf_module /usr/lib/apache2/modules/mod_rpaf.so" > /etc/apache2/mods-available/rpaf.load && a2enmod rpaf

# configure cron
RUN rm -rf /var/spool/cron && ln -s /run/cron /var/spool/cron && \
    rm -rf /var/spool/postfix && ln -s /run/postfix /var/spool/postfix

# clear out the crontab
RUN rm -f /etc/cron.d/* /etc/cron.daily/* /etc/cron.hourly/* /etc/cron.monthly/* /etc/cron.weekly/* && truncate -s0 /etc/crontab

# lock www-data but allow su - www-data to work
RUN passwd -l www-data && usermod --shell /bin/bash --home /app/data www-data

#Start Pixelfed here
RUN  mkdir -p /app/code/pkg
RUN	curl -L https://github.com/pixelfed/pixelfed/archive/v0.10.9.tar.gz | tar -xz --strip-components=1 -C /app/code/pkg

#Finish Pixelfed here
RUN	cd /app/code/pkg && \
	composer install --no-ansi --no-interaction --optimize-autoloader && \
	chown -R www-data:www-data /app/code/pkg && \
	chmod -R g+rw /app/code/pkg && \
	find . -type d -exec chmod 755 {} \; && \
	find . -type f -exec chmod 644 {} \;

ENV ARTISAN="sudo -E -u www-data php /app/code/pkg/artisan"
RUN $ARTISAN horizon:install
RUN $ARTISAN horizon:assets

# configure supervisor
ADD supervisor/ /etc/supervisor/conf.d/
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf

COPY start.sh /app/code/

# Prepare everything to place config under /app/data
COPY .env /app/code/pkg/env.production
RUN ln -fs /app/data/env.production /app/code/pkg/.env

## TODO: move less stuff (like storage/app)
RUN mv /app/code/pkg/storage /app/code/pkg/storage.orig
RUN mv /app/code/pkg/bootstrap/cache /app/code/pkg/bootstrap_cache.orig
## Link stuff I moved to it's original place
RUN ln -s /app/data/storage /app/code/pkg/storage
RUN ln -s /app/data/bootstrap_cache /app/code/pkg/bootstrap/cache
## replace call to `artisan storage:link`
RUN ln -s /app/code/pkg/storage/app/public /app/code/pkg/public/storage


CMD [ "/app/code/start.sh" ]
