#!/bin/bash

set -eu

mkdir -p /app/data/public /run/apache2 /run/cron /run/app/sessions /run/postfix
mkdir -p /run/pixelfed/sessions /run/pixelfed/bootstrap-cache /run/pixelfed/framework-cache /run/pixelfed/logs /run/pixelfed/cache /run/postfix/maildrop
chown -R www-data:www-data /run/postfix

# Copy .env to /app/data to make it editable by the user
if [[ ! -f /app/data/env.production ]]; then
    echo "==> Copying env on first run"
    cp /app/code/pkg/env.production /app/data/env.production
fi


if [ ! -d /app/data/storage ]; then
  cp -r /app/code/pkg/storage.orig /app/data/storage
fi
if [ ! -d /app/data/bootstrap_cache ]; then
  cp -r /app/code/pkg/bootstrap_cache.orig /app/data/bootstrap_cache
fi
# Creating a secret APP_KEY
if [ ! -f /app/data/APP_KEY ]; then
  APP_KEY="base64:$(dd if=/dev/urandom bs=32 count=1 | base64)"
  echo $APP_KEY > /app/data/APP_KEY
else
  APP_KEY="$(cat /app/data/APP_KEY)"
fi
export APP_KEY

chown -R www-data:www-data /app/data /run/apache2 /run/app /tmp

$ARTISAN config:cache
$ARTISAN route:cache
$ARTISAN view:cache
$ARTISAN migrate --force
$ARTISAN update

echo "==> Starting Pixelfed"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Lamp
