<p align="center"><img src="https://pixelfed.nyc3.cdn.digitaloceanspaces.com/logos/pixelfed-full-color.svg" width="300px"></p>

## Introduction

A free and ethical photo sharing platform, powered by ActivityPub federation.

<p align="center">
<img src="https://pixelfed.nyc3.cdn.digitaloceanspaces.com/media/Screen%20Shot%202019-09-08%20at%2010.40.54%20PM.png">
</p>

## Official Documentation

Documentation for Pixelfed can be found on the [Pixelfed documentation website](https://docs.pixelfed.org/).

## License

Pixelfed is open-sourced software licensed under the AGPL license.
