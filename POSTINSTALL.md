Self registration is enabled.
There is no default admin, go register and then see ARTISAN below to make yourself an admin.

If you need to run Artisan (to make someone an admin, etc):
Open Terminal
Run: $ARTISAN <commands>
If you run $ARTISAN by itself, it will tell you what it can do.

Example to add the first user (preseumably you) as admin:
$ARTISAN user:admin 1

You can find the configuration file (env.production) under /app/data.
Don`t forget to restart the app after making any changes to this file.